package jeroomlamote.be.airsoftarea.Fragments;

import android.support.annotation.RequiresApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import jeroomlamote.be.airsoftarea.Helpers.DatabaseHelper;
import jeroomlamote.be.airsoftarea.Helpers.DisplayerHelper;
import jeroomlamote.be.airsoftarea.Model.User;
import jeroomlamote.be.airsoftarea.R;


public class RegisterFragment extends Fragment  {

    private OnFragmentRegisterInteractionListener mListener;
    private View view;
    private DisplayerHelper dh;
    private ProgressDialog progressDialog;
    private DatabaseHelper myDb;
    private boolean useParse;
    private boolean usernameExist;
    private EditText et_username, et_password, et_email;

    public RegisterFragment() {

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_register, container, false);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        myDb = new DatabaseHelper(getActivity());
        dh = new DisplayerHelper(getActivity());
        useParse = prefs.getBoolean("useParse", false);
        progressDialog = new ProgressDialog(getActivity());

        Button btnRegister = (Button) view.findViewById(R.id.fragment_register_btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });

        TextView info = (TextView) view.findViewById(R.id.fragment_register_tvInfo);
        if (useParse) {
            info.setText(R.string.RegisterFragment_tvInfo);

        } else {
            info.setText(R.string.RegisterFragment_tvInfo_2);
        }

        return view;
    }


    public void registerUser() {
        progressDialog.setMessage("Please Wait");
        progressDialog.setTitle("Registering in");
        progressDialog.show();

        et_username = (EditText) view.findViewById(R.id.fragment_register_etUsername);
        et_password = (EditText) view.findViewById(R.id.fragment_register_etPassword);
        et_email = (EditText) view.findViewById(R.id.fragment_register_etEmail);

        if (useParse) {
            //Nieuw user aanmaken.
            ParseUser puser = new ParseUser();
            puser.setUsername(et_username.getText().toString());
            puser.setPassword(et_password.getText().toString());
            puser.setEmail(et_email.getText().toString());

            //user saven.
            puser.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        progressDialog.dismiss();
                        dh.alertDisplayer("Progress", "Succesfully registrated");
                        mListener.navigateFromRegisterFragment();
                    } else {
                        progressDialog.dismiss();
                        String message;
                        //Vanaf de ":" de error weergeven
                        if (e.getMessage().toLowerCase().contains(":")) {
                            int pos = e.getMessage().toLowerCase().indexOf(":");
                            message = e.getMessage().substring(pos + 1);
                        } else {
                            message = e.getMessage();
                        }
                        dh.alertDisplayer("Progress", "Something went wrong.\n" + message);
                    }
                }
            });
        } else {
            try {
                if (!checkIfFieldsAreEmpty()) {

                    if (!checkIfAccountAlreadyExists()) {
                        myDb.insertUser(et_username.getText().toString(), et_password.getText().toString(), et_email.getText().toString());
                        progressDialog.dismiss();
                        dh.alertDisplayer("Progress", "Succesfully registrated");
                        mListener.navigateFromRegisterFragment();
                    }

                }
            } catch (Exception e) {
                progressDialog.dismiss();
                dh.alertDisplayer("Registration failed", "Fields must be filled and email must exist!");
            }
        }
    }

    private boolean checkIfAccountAlreadyExists() {
        Cursor res = myDb.getAllUserTableData();
        if (res.getCount() == 0) {
            return false;
        } else {

            while (res.moveToNext()) {
                String username = res.getString(1);
                String email = res.getString(3);

                if (et_username.getText().toString().equals(username)) {
                    progressDialog.dismiss();
                    dh.alertDisplayer("Progress", "Something went wrong.\n" + "Account already exists for this username.");
                    return true;
                }

                if (et_email.getText().toString().equals(email)) {
                    progressDialog.dismiss();
                    dh.alertDisplayer("Progress", "Something went wrong.\n" + "Account already exists for this emailaddress.");
                    return true;
                }

            }
        }
        return false;
    }

    private boolean checkIfFieldsAreEmpty() {
        if (et_username.getText().toString().equals("")) {
            progressDialog.dismiss();
            dh.alertDisplayer("Progress", "Something went wrong.\n" + "Username cannot be missing or blank.");
            return true;
        }
        if (et_password.getText().toString().equals("")) {
            progressDialog.dismiss();
            dh.alertDisplayer("Progress", "Something went wrong.\n" + "Password cannot be missing or blank.");
            return true;
        }
        if (et_email.getText().toString().equals("")) {
            progressDialog.dismiss();
            dh.alertDisplayer("Progress", "Something went wrong.\n" + "Email cannot be missing or blank.");
            return true;
        }
        return false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentRegisterInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentRegisterInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentRegisterInteractionListener {
        void navigateFromRegisterFragment();
    }
}
