package jeroomlamote.be.airsoftarea.Fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import jeroomlamote.be.airsoftarea.Activities.SecondActivity;
import jeroomlamote.be.airsoftarea.Helpers.DatabaseHelper;
import jeroomlamote.be.airsoftarea.Helpers.DisplayerHelper;
import jeroomlamote.be.airsoftarea.Helpers.LocationHelper;
import jeroomlamote.be.airsoftarea.Model.User;
import jeroomlamote.be.airsoftarea.R;


public class AddLocationFragment extends Fragment implements LocationHelper.LocationHelperInterface {

    private OnFragmentAddLocationInteractionListener mListener;
    public double longitude = 0;
    public double latitude = 0;
    private ParseObject placeObject;
    private Location location;
    private DisplayerHelper dh;
    private DatabaseHelper myDb;
    private ProgressDialog progressDialog;
    private User userSqlite;
    private Boolean useParse;
    public boolean dubbelelocatie;

    public AddLocationFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_locations_add, container, false);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        useParse = prefs.getBoolean("useParse", false);

        //Gebruiker via activity aan fragment doorgeven bij gebruik sqlite.
        if(!useParse) {
            SecondActivity sa = (SecondActivity) getActivity();
            userSqlite = sa.userSqlite;
        }

        dh = new DisplayerHelper(getActivity());
        progressDialog = new ProgressDialog(getActivity());
        myDb = new DatabaseHelper(getActivity());
        placeObject = new ParseObject("AirsoftLocation");

        Button btnRequest = (Button) view.findViewById(R.id.fragment_locations_add_btnAddLocation);
        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonAddLocationClicked();
            }
        });
        return view;
    }


    private void onButtonAddLocationClicked() {
        progressDialog.setMessage("Please Wait");
        progressDialog.setTitle("Adding location...");
        progressDialog.show();

        try {
            //Locatie opvragen via locationmanager.
            location = new Location("3");
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (location != null) {
                longitude = location.getLongitude();
                latitude = location.getLatitude();
                //We controleren of de locatie al bestaat.
                if (useParse) {
                    //Omdat useParse dit proces in de background runt moeten we wachten op het resultaat en doe ik dit aan de hand van LocationHelper.
                    LocationHelper locationHelper = new LocationHelper();
                    locationHelper.listener = this;
                    locationHelper.setLatitudeandLongitude(latitude, longitude);
                    locationHelper.checkIfLocationAlreadyExist();
                } else {
                    //Coordinaten doorgeven aan de functie die controleert of de locatie al bestaat.
                    addLocationSqlite(longitude, latitude);
                }
            } else {
                progressDialog.dismiss();
                dh.alertDisplayer("Alert", "Something went wrong!\nTurn on your gps and try again.");
            }
        } catch (SecurityException e) {
            Log.d("DEBUG", e.getMessage());
        }
    }

    public void addLocationSqlite(double longitude, double latitude) {
        float distance = 0f;

        Cursor res = myDb.getAllAirsoftLocationTableData();
        //Als er geen locaties inzitten kunnen we direct toevoegen
        if (res.getCount() == 0) {
            distance = 51;
        } else {
            while (res.moveToNext()) {
                Location locationE = new Location("point E");
                locationE.setLatitude(latitude);
                locationE.setLongitude(longitude);

                Location locationF = new Location("point F");
                locationF.setLatitude(Double.parseDouble(res.getString(2)));
                locationF.setLongitude(Double.parseDouble(res.getString(3)));

                distance = locationE.distanceTo(locationF);
            }
        }

        //Als er een afstand is die kleiner is dan 50m, is de locatie al toegevoegd.
        if (distance < 50f) {
            progressDialog.dismiss();
            dh.alertDisplayer("Location already added!", "Location is 50 meters near you");
        } else {
            myDb.insertAirsoftLocation(longitude, latitude, userSqlite.getEmail());
            progressDialog.dismiss();
            dh.alertDisplayer("Location has been added!", "Coordinates: \n" + "Longitude: " + location.getLongitude() + "\nLatitude: " + location.getLatitude());
        }
    }

    @Override
    public void setLocationExist(Boolean dubbelelocatie) {
        //Als dubbele locatie true is, dan is de locatie al toegevoegd! Ander kunnen we de locatie toevoegen.
        this.dubbelelocatie = dubbelelocatie;
        if (!this.dubbelelocatie) {
            ParseGeoPoint point = new ParseGeoPoint(latitude, longitude);
            placeObject.put("Location", point);
            placeObject.saveInBackground();
            progressDialog.dismiss();
            dh.alertDisplayer("Location has been added!", "Coordinates: \n" + "Longitude: " + location.getLongitude() + "\nLatitude: " + location.getLatitude());
        } else {
            progressDialog.dismiss();
            dh.alertDisplayer("Location already added!", "Location is 50 meters near you");
        }
    }

    @Override
    public void onAttach(Context context) {
        Log.d("D", "onAttachFourthFragment");
        super.onAttach(context);
        try {
            mListener = (OnFragmentAddLocationInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentAddLocationInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Log.d("D", "onDetachFourthFragment");
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentAddLocationInteractionListener {

    }
}
