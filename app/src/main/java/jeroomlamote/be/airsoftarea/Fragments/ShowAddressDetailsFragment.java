package jeroomlamote.be.airsoftarea.Fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import jeroomlamote.be.airsoftarea.Model.AddressClass;
import jeroomlamote.be.airsoftarea.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ShowAddressDetailsFragment extends Fragment {

    private AddressClass address;

    public ShowAddressDetailsFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_show_address_details, container, false);
            ((TextView) view.findViewById(R.id.fragment_show_address_details_tvStreet)).setText(address.getStreet());
            ((TextView) view.findViewById(R.id.fragment_show_address_details_tvGemeente)).setText(address.getCity());
            ((TextView) view.findViewById(R.id.fragment_show_address_details_tvLand)).setText(address.getCountry());
            ((TextView) view.findViewById(R.id.fragment_show_address_details_tvPostcode)).setText(address.getPostalCode());
            return view;
    }

    public  void setAddress(AddressClass address){
        this.address = address;
    }

}
