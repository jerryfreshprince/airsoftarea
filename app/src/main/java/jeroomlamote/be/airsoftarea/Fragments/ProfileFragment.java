package jeroomlamote.be.airsoftarea.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import jeroomlamote.be.airsoftarea.Activities.SecondActivity;
import jeroomlamote.be.airsoftarea.Helpers.DatabaseHelper;
import jeroomlamote.be.airsoftarea.Helpers.DisplayerHelper;
import jeroomlamote.be.airsoftarea.Model.User;
import jeroomlamote.be.airsoftarea.R;


public class ProfileFragment extends Fragment {

    private OnFragmentProfileInteractionListener mListener;
    private EditText et_username;
    private EditText et_email;
    private EditText et_pasword;
    private ParseUser currentUser;
    private DisplayerHelper dh;
    private ProgressDialog progressDialog;
    private SharedPreferences prefs;
    private DatabaseHelper myDb;
    private User userSqlite;
    private boolean useParse;

    public ProfileFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        myDb = new DatabaseHelper(getActivity());
        dh = new DisplayerHelper(getActivity());
        progressDialog = new ProgressDialog(getActivity());

        et_username = (EditText) view.findViewById(R.id.fragment_profile__etUsername);
        et_email = (EditText) view.findViewById(R.id.fragment_profile__etEmail);
        et_pasword = (EditText) view.findViewById(R.id.fragment_profile__etPassword);
        et_pasword.setTypeface(Typeface.DEFAULT);

        SecondActivity sa = (SecondActivity) getActivity();
        userSqlite = sa.userSqlite;

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        useParse = prefs.getBoolean("useParse", false);

        //Vul gegevens al in zodat het makkelijker aan te passen is
        if (useParse) {
            currentUser = ParseUser.getCurrentUser();

            if (currentUser != null) {
                et_username.setText(currentUser.getUsername());
                et_email.setText(currentUser.getEmail());
            }

        } else {

            if (userSqlite != null) {
                et_username.setText(userSqlite.getUsername());
                et_email.setText(userSqlite.getEmail());
            }

        }

        Button btnSave = (Button) view.findViewById(R.id.fragment_profile__btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveProfile();
            }
        });

        return view;
    }

    //Gegevens opslaan
    public void saveProfile() {
        progressDialog.setMessage("Please Wait");
        progressDialog.setTitle("Saving changes");
        progressDialog.show();

        useParse = prefs.getBoolean("useParse", false);

        //Via parse
        if (useParse) {
            currentUser = ParseUser.getCurrentUser();

            if (currentUser != null) {
                currentUser.setUsername(et_username.getText().toString());
                currentUser.setEmail(et_email.getText().toString());

                if (!et_pasword.getText().toString().equals("")) {
                    currentUser.setPassword(et_pasword.getText().toString());
                }

                currentUser.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (null == e) {
                            progressDialog.dismiss();
                            dh.alertDisplayer("Progress", "Successfully saved");
                            mListener.changeHeaderText();
                        } else {
                            progressDialog.dismiss();
                            dh.alertDisplayer("Progress", "Could not save changes");
                        }
                    }
                });

            } else {
                progressDialog.dismiss();
                dh.alertDisplayer("Progress", "Something went wrong!");
            }

        } else { //Via sqlite
            if (userSqlite != null) {

                userSqlite.setUsername(et_username.getText().toString());
                userSqlite.setEmail(et_email.getText().toString());
                if (!et_pasword.getText().toString().equals("")) {
                    userSqlite.setPassword(et_pasword.getText().toString());
                }
                myDb.updateUser(userSqlite);

                progressDialog.dismiss();
                dh.alertDisplayer("Progress", "Successfully saved");
                mListener.changeHeaderText();
            } else {
                progressDialog.dismiss();
                dh.alertDisplayer("Progress", "Something went wrong!");
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentProfileInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentProfileInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentProfileInteractionListener {
        void changeHeaderText();
    }
}
