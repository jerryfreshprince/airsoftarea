package jeroomlamote.be.airsoftarea.Fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import jeroomlamote.be.airsoftarea.Helpers.DatabaseHelper;
import jeroomlamote.be.airsoftarea.Model.AddressClass;
import jeroomlamote.be.airsoftarea.Adapters.AddressClassAdapter;
import jeroomlamote.be.airsoftarea.Activities.MapsActivity;
import jeroomlamote.be.airsoftarea.R;

public class LocationsListFragment extends Fragment {

    private OnFragmenLocationsListInteractionListener mListener;
    public double longitude,latitude;
    private List<double[]> fetched_data;
    private ArrayList<AddressClass> addresslist;
    private ProgressDialog progressDialog;
    private AddressClass address;
    private ListView lv;
    private Boolean useParse;
    private DatabaseHelper myDb;

    public LocationsListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_locations_list, container, false);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        useParse = prefs.getBoolean("useParse", false);
        myDb = new DatabaseHelper(getActivity());
        progressDialog = new ProgressDialog(getActivity());
        lv = (ListView) view.findViewById(R.id.lvAdresses);

        //De lijst vullen
        fillFetchedDataAndAdresses();

        //als er op een item geklikt wordt dan worden gegevens getoond
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AddressClass address = (AddressClass) parent.getAdapter().getItem(position);
                navigateToShowDetails(address);
            }
        });

        Button btnGoogleMaps = (Button) view.findViewById(R.id.btnGoogleMaps);
        btnGoogleMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onButtonGoogleMapsPressed();
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmenLocationsListInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnFragmenLocationsListInteractionListener");
        }
    }

    //Naar de detailsfragment gaan
    public void navigateToShowDetails(AddressClass address){
        ShowAddressDetailsFragment showMemberDetailsFragment = new ShowAddressDetailsFragment();
        showMemberDetailsFragment.setAddress(address);
        getFragmentManager().beginTransaction().replace(R.id.content_frame, showMemberDetailsFragment).addToBackStack(null)
                .commit();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    //list vullen
    private void fillFetchedDataAndAdresses() {
        fetched_data = new ArrayList<>();
        addresslist = new ArrayList<>();
        progressDialog = ProgressDialog.show(getActivity(),"Loading addresses","Please Wait",true);

        //via parse
        if(useParse) {
            ParseGeoPoint location = new ParseGeoPoint(latitude,longitude);
            ParseQuery<ParseObject> query = ParseQuery.getQuery("AirsoftLocation");
            query.whereNear("Location",location);
            query.setLimit(8);
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> objects, ParseException e) {

                    Geocoder geocoder;
                    List<Address> geocodeaddress;
                    geocoder = new Geocoder(getActivity(), Locale.getDefault());
                    fetched_data.clear();

                    for(int i= 0 ;i<objects.size();i++){
                        double tempLat = objects.get(i).getParseGeoPoint("Location").getLatitude();
                        double tempLng = objects.get(i).getParseGeoPoint("Location").getLongitude();
                        double[] temp = new double[] {tempLat,tempLng};
                        fetched_data.add(temp);

                        try {
                            //Adressen vullen met informatie aan de hand van de coördinaten.
                            geocodeaddress = geocoder.getFromLocation(tempLat, tempLng, 1);
                            address = new AddressClass(geocodeaddress.get(0).getAddressLine(0),geocodeaddress.get(0).getLocality(),geocodeaddress.get(0).getCountryName(),geocodeaddress.get(0).getPostalCode());
                            addresslist.add(address);
                            AddressClassAdapter addressAdapter = new AddressClassAdapter(getActivity(), addresslist);
                            lv.setAdapter(addressAdapter);
                        } catch (IOException ex) {
                            Log.d("D", ex.getMessage());
                        }

                    }

                    progressDialog.dismiss();
                }
            });
        } else { //via sqlite
            //Alle locaties uit de sqlite database halen
            Cursor res = myDb.getAllAirsoftLocationTableData();
            if (res.getCount() == 0) {
                Log.d("D", "No locations in table");
            } else {

                Geocoder geocoder;
                List<Address> geocodeaddress;
                geocoder = new Geocoder(getActivity(), Locale.getDefault());
                fetched_data.clear();

                while (res.moveToNext()) {
                    double tempLat = Double.parseDouble(res.getString(2));
                    double tempLng = Double.parseDouble(res.getString(3));
                    double[] temp = new double[] {tempLat,tempLng};
                    fetched_data.add(temp);

                    try {
                        //Adressen vullen met informatie aan de hand van de coördinaten.
                        geocodeaddress = geocoder.getFromLocation(tempLat, tempLng, 1);
                        address = new AddressClass(geocodeaddress.get(0).getAddressLine(0),geocodeaddress.get(0).getLocality(),geocodeaddress.get(0).getCountryName(),geocodeaddress.get(0).getPostalCode());
                        addresslist.add(address);
                        AddressClassAdapter addressAdapter = new AddressClassAdapter(getActivity(), addresslist);
                        lv.setAdapter(addressAdapter);
                    } catch (IOException ex) {
                        Log.d("D", ex.getMessage());
                    }

                }

                progressDialog.dismiss();
            }
        }
    }


    private void onButtonGoogleMapsPressed() {
        fetched_data = new ArrayList<>();
        progressDialog = ProgressDialog.show(getActivity(),"Fetching Nearest Location","Please Wait",true);

        if(useParse) {
            ParseGeoPoint location = new ParseGeoPoint(latitude,longitude);
            ParseQuery<ParseObject> query = ParseQuery.getQuery("AirsoftLocation");
            query.whereNear("Location",location);
            query.setLimit(8);
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> objects, ParseException e) {

                    fetched_data.clear();

                    //Locaties vullen die door googlemaps weergegeven moeten worden
                    for(int i= 0 ;i<objects.size();i++){
                        double tempLat = objects.get(i).getParseGeoPoint("Location").getLatitude();
                        double tempLng = objects.get(i).getParseGeoPoint("Location").getLongitude();
                        double[] temp = new double[] {tempLat,tempLng};
                        fetched_data.add(temp);
                    }

                    progressDialog.dismiss();
                    startGoogleMap();
                }
            });
        } else {
            Cursor res = myDb.getAllAirsoftLocationTableData();

            if (res.getCount() == 0) {
                Log.d("D", "No locations in table");
            } else {

                fetched_data.clear();

                //Locaties vullen die door googlemaps weergegeven moeten worden
                while (res.moveToNext()) {
                    double tempLat = Double.parseDouble(res.getString(2));
                    double tempLng = Double.parseDouble(res.getString(3));
                    double[] temp = new double[] {tempLat,tempLng};
                    fetched_data.add(temp);
                }

                progressDialog.dismiss();
                startGoogleMap();
            }
        }

    }

    void startGoogleMap(){
        Intent i = new Intent(getActivity(),MapsActivity.class);
        i.putExtra("location",(Serializable)fetched_data);
        startActivity(i);
    }

    public interface OnFragmenLocationsListInteractionListener {

    }
}
