package jeroomlamote.be.airsoftarea.Fragments;

import android.support.annotation.RequiresApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import java.io.IOException;
import jeroomlamote.be.airsoftarea.Helpers.DatabaseHelper;
import jeroomlamote.be.airsoftarea.Helpers.DisplayerHelper;
import jeroomlamote.be.airsoftarea.Model.User;
import jeroomlamote.be.airsoftarea.R;

public class LoginFragment extends Fragment {

    private OnFragmentLoginInteractionListener mListener;
    private View view;
    private ProgressDialog progressDialog;
    private DisplayerHelper dh;
    private Switch aSwitch;
    private SharedPreferences prefs;
    private DatabaseHelper myDb;
    private User user;
    private Boolean useParse;
    private EditText et_username;
    private EditText et_password;

    public LoginFragment() {

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_login, container, false);
        myDb = new DatabaseHelper(getContext());
        dh = new DisplayerHelper(getActivity());
        progressDialog = new ProgressDialog(getActivity());
        user = new User();
        prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        ParseUser.getCurrentUser();
        ParseUser.logOut();

        ImageView btnInfo = (ImageView) view.findViewById(R.id.fragment_login_ivInfo);
        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dh.alertDisplayer("Info", getString(R.string.alertDisplayerSwitchInfo));
            }
        });

        Button btnLogin = (Button) view.findViewById(R.id.fragment_login_btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setMessage("Please Wait");
                progressDialog.setTitle("Logging in");
                progressDialog.show();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            loginUser();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });

        TextView tvRegistration = (TextView) view.findViewById(R.id.fragment_login_tvRegistration);
        tvRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.navigateFromLoginFragment();
            }
        });

        aSwitch = (Switch) view.findViewById(R.id.fragment_login_switchUsage);
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    //Controle of men wel met het internet kan verbinden.
                    if (!isOnline()) {
                        //Indien niet de switch op false zetten.
                        prefs.edit().putBoolean("useParse", false).apply();
                        dh.alertDisplayer("Internet connection", "You are not connected. Your account will be stored locally");
                        aSwitch.setChecked(false);
                    } else {
                        prefs.edit().putBoolean("useParse", true).apply();
                    }
                } else {
                    prefs.edit().putBoolean("useParse", false).apply();
                }

                useParse = prefs.getBoolean("useParse", false);
            }
        });

        //Bij het starten de switch op "useParse" zetten.
        useParse = prefs.getBoolean("useParse", false);
        aSwitch.setChecked(useParse);

        return view;
    }

    public void loginUser() {
        useParse = prefs.getBoolean("useParse", false);
        et_username = (EditText) view.findViewById(R.id.fragment_login_etUsername);
        et_password = (EditText) view.findViewById(R.id.fragment_login_etPassword);

        if (useParse) {
            //Inloggen via parse.
            ParseUser.logInInBackground(et_username.getText().toString(), et_password.getText().toString(), new LogInCallback() {
                @Override
                public void done(ParseUser parseUser, ParseException e) {
                    if (parseUser != null) {
                        progressDialog.dismiss();
                        dh.alertDisplayer("Login Successful", "Welcome " + parseUser.getUsername());
                        mListener.startSecondActivity();
                    } else {
                        progressDialog.dismiss();
                        dh.alertDisplayer("Login failed", "Username or password is invalid");
                    }
                }
            });
        } else {
            //Inloggen via sqlite.
            user = myDb.getUser(et_username.getText().toString());

            if (user != null) {
                //Controleren of paswoord klopt.
                if (user.getPassword().equals(et_password.getText().toString())) {

                    //Moet het op de UI Thread runnen om volgende exception af te handelen: Can't create handler inside thread that has not called Looper.prepare().
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            progressDialog.dismiss();
                            dh.alertDisplayer("Login Successful", "Welcome " + et_username.getText().toString());
                        }
                    });

                    mListener.startSecondActivityWithUsername(user.getUsername());
                } else {

                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            progressDialog.dismiss();
                            dh.alertDisplayer("Login failed", "Password incorrect, try again");
                        }
                    });
                }
            } else {

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        progressDialog.dismiss();
                        dh.alertDisplayer("Login failed", "User not found, try an other username");
                    }
                });

            }
        }
    }

    //Check als er internetconnectie is door te pingen naar de server.
    public boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }


    @Override
    public void onAttach(Context context) {
        Log.d("D", "onAttachFirstFragment");
        super.onAttach(context);
        try {
            mListener = (OnFragmentLoginInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnFragmentLoginInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Log.d("D", "onDetachFirstFragment(");
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentLoginInteractionListener {
        void navigateFromLoginFragment();

        void startSecondActivityWithUsername(String username);

        void startSecondActivity();
    }
}
