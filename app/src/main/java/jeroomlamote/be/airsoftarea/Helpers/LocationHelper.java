package jeroomlamote.be.airsoftarea.Helpers;

import android.location.Location;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import java.util.List;

/**
 * Created by jeroom on 24/04/2017.
 */

public class LocationHelper {

    private double longitude, latitude;
    private boolean dubbelelocatie;
    private float distance;
    public LocationHelperInterface listener;

    public interface LocationHelperInterface {
        void setLocationExist(Boolean dubbelelocatie);
    }

    public void checkIfLocationAlreadyExist() {
        dubbelelocatie = false;
        distance = 0f;
        ParseGeoPoint currentlocation = new ParseGeoPoint(latitude, longitude);
        ParseQuery<ParseObject> query = ParseQuery.getQuery("AirsoftLocation");
        query.whereNear("Location", currentlocation);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (objects.size() != 0) {

                    int i = 0;
                    do {
                        Location locationA = new Location("point A");
                        locationA.setLatitude(latitude);
                        locationA.setLongitude(longitude);

                        Location locationB = new Location("point B");
                        double tempLat = objects.get(i).getParseGeoPoint("Location").getLatitude();
                        double tempLng = objects.get(i).getParseGeoPoint("Location").getLongitude();
                        locationB.setLatitude(tempLat);
                        locationB.setLongitude(tempLng);

                        //distance berekenen
                        distance = locationA.distanceTo(locationB);
                        i++;
                    } while (distance > 50f && i < objects.size());

                } else {
                    distance = 51f;
                }

                dubbelelocatie = distance < 50f;

                listener.setLocationExist(dubbelelocatie);
            }
        });
    }

    public void setLatitudeandLongitude(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
