package jeroomlamote.be.airsoftarea.Helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.ContentValues;
import android.database.Cursor;
import jeroomlamote.be.airsoftarea.Model.User;
import jeroomlamote.be.airsoftarea.Model.AirsoftLocation;


/**
 * Created by jeroom on 20/03/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 11;
    private static  final  String DATABASE_NAME = "airsoftarea.db";

    private SQLiteDatabase db;
    private Cursor res;

    private static final String TABLE_USER = "User";
    private static final String COL_ID_TABLE_USER =  "_id";
    private static final String COL_USERNAME_TABLE_USER  = "username";
    private static final String COL_PASSWORD_TABLE_USER  = "password";
    private static final String COL_EMAIL_TABLE_USER  = "email";

    private static final String TABLE_AIRSOFTLOCATION = "AirsoftLocation";
    private static final String COL_ID_TABLE_AIRSOFTLOCATION =  "_id";
    private static final String COL_CREATOR_TABLE_AIRSOFTLOCATION =  "creator";
    private static final String COL_LATITUDE_TABLE_AIRSOFTLOCATION  = "latitude";
    private static final String COL_LONGITUDE_TABLE_AIRSOFTLOCATION  = "longitude";

    private static final String CREATEUSERTABLE = "create table "
            + TABLE_USER + "("
            + COL_ID_TABLE_USER + " integer primary key autoincrement, "
            + COL_USERNAME_TABLE_USER + " text not null, "
            + COL_PASSWORD_TABLE_USER + " text not null, "
            + COL_EMAIL_TABLE_USER + " text not null)";

    private static final String CREATEAIRSOFTLOCATIONTABLE = "create table "
            + TABLE_AIRSOFTLOCATION + " ("
            + COL_ID_TABLE_AIRSOFTLOCATION + " integer primary key autoincrement, "
            + COL_CREATOR_TABLE_AIRSOFTLOCATION + " integer, "
            + COL_LATITUDE_TABLE_AIRSOFTLOCATION + " real not null, "
            + COL_LONGITUDE_TABLE_AIRSOFTLOCATION + " real not null, "
            +"FOREIGN KEY (" + COL_CREATOR_TABLE_AIRSOFTLOCATION + ") REFERENCES " + COL_USERNAME_TABLE_USER + "(" + TABLE_USER + "))";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATEUSERTABLE);
        db.execSQL(CREATEAIRSOFTLOCATIONTABLE);
        this.db = db;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String queryDropUserTable = "DROP TABLE IF EXISTS " + TABLE_USER;
        String queryDropAirsoftLocationTable = "DROP TABLE IF EXISTS " + TABLE_AIRSOFTLOCATION;
        db.execSQL(queryDropUserTable);
        db.execSQL(queryDropAirsoftLocationTable);
        this.onCreate(db);
    }

    public void insertUser(String username, String password, String email){
        User user = new User(username, password, email);

        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COL_USERNAME_TABLE_USER, user.getUsername());
        values.put(COL_PASSWORD_TABLE_USER, user.getPassword());
        values.put(COL_EMAIL_TABLE_USER, user.getEmail());

        db.insert(TABLE_USER, null, values);
        db.close();
    }


    public User getUser(String username) {
        db = this.getWritableDatabase();
        res = db.rawQuery("select * from "+TABLE_USER+" where "+COL_USERNAME_TABLE_USER+" = '" + username + "'", null);
        String n;
        User returnUser = null;
        if(res.moveToFirst()){
            do{
                n = res.getString(1);

                if(n.equals(username)){
                    User user = new User();
                    user.setUsername(res.getString(1));
                    user.setPassword(res.getString(2));
                    user.setEmail(res.getString(3));
                    returnUser = user;
                }

            } while(res.moveToNext());

            res.close();
        }
        return returnUser;
    }

    public void updateUser(User u) {
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COL_ID_TABLE_USER, u.getObjectId());
        values.put(COL_USERNAME_TABLE_USER, u.getUsername());
        values.put(COL_PASSWORD_TABLE_USER, u.getPassword());
        values.put(COL_EMAIL_TABLE_USER, u.getEmail());

        db.update(TABLE_USER, values, "_id = ?", new String[] { u.getObjectId() });
        db.close();
    }

    public Cursor getAllUserTableData() {
        db = this.getWritableDatabase();
        res = db.rawQuery("select * from "+TABLE_USER,null);
        return res;
    }


    public void insertAirsoftLocation(double longitude, double latitude, String useremail){
        AirsoftLocation al = new AirsoftLocation(longitude, latitude, useremail);

        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COL_CREATOR_TABLE_AIRSOFTLOCATION, al.getCreator());
        values.put(COL_LATITUDE_TABLE_AIRSOFTLOCATION, al.getLatitude());
        values.put(COL_LONGITUDE_TABLE_AIRSOFTLOCATION, al.getLongitude());

        db.insert(TABLE_AIRSOFTLOCATION, null, values);
        db.close();
    }

    public Cursor getAllAirsoftLocationTableData() {
        db = this.getWritableDatabase();
        res = db.rawQuery("select * from "+TABLE_AIRSOFTLOCATION,null);
        return res;
    }
}
