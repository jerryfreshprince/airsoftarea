package jeroomlamote.be.airsoftarea.Helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;

/**
 * Created by jeroom on 25/04/2017.
 */

//klasse om alertbox weer te geven (wegens meermaals gebruik)
public class DisplayerHelper   {
    private Activity activity;

    public DisplayerHelper(Activity activity){
        this.activity = activity;
    }

    public void alertDisplayer(String title,String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("DEBUG", "onClick");
                        dialog.dismiss();
                    }
                });
        AlertDialog ok = builder.create();
        ok.show();
    }
}
