package jeroomlamote.be.airsoftarea.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import jeroomlamote.be.airsoftarea.Model.AddressClass;
import jeroomlamote.be.airsoftarea.R;

/**
 * Created by jeroom on 7/04/2017.
 */

public class AddressClassAdapter extends BaseAdapter {

    private ArrayList<AddressClass> addresses;
    private LayoutInflater layoutInflater;

    private static class ViewHolder {
        private ImageView imvNavigation;
        private TextView tvAddressStreet;
        private TextView tvAddressCity;
    }

    public AddressClassAdapter(Context context, ArrayList<AddressClass> addresses) {
        this.addresses = addresses;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return addresses.size();
    }

    @Override
    public Object getItem(int position) {
        return addresses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //ViewHolder voor het weergeven van deze objecten in een list
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.listviewitem_address, null);
            viewHolder = new ViewHolder();
            viewHolder.imvNavigation = (ImageView) convertView.findViewById(R.id.listviewitem_address_imv);
            viewHolder.tvAddressStreet = (TextView) convertView.findViewById(R.id.listviewitem_address_tvStreet);
            viewHolder.tvAddressCity = (TextView) convertView.findViewById(R.id.listviewitem_address_tvCity);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //informatie weergeven in per listitem
        AddressClass address = addresses.get(position);
        viewHolder.imvNavigation.setImageResource(R.mipmap.ic_drawer_header_icon);
        viewHolder.tvAddressStreet.setText(address.getStreet());
        viewHolder.tvAddressCity.setText(address.getCity());

        return convertView;
    }
}
