package jeroomlamote.be.airsoftarea.Model;

/**
 * Created by jeroom on 7/04/2017.
 */

public class AddressClass {

    private String street;
    private String city;
    private String country;
    private String postalCode ;

    public AddressClass() {

    }

    public AddressClass(String street, String city, String country, String postalCode) {
        this.street = street;
        this.city = city;
        this.country = country;
        this.postalCode = postalCode;
    }

    public String getStreet() {
        return street;
    }
    public String getCity() {
        return city;
    }
    public String getCountry() {
        return country;
    }
    public String getPostalCode() {
        return postalCode;
    }
}
