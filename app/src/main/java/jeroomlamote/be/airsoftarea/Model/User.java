package jeroomlamote.be.airsoftarea.Model;

/**
 * Created by jeroom on 26/04/2017.
 */

public class User {
    private int objectId;
    private String username;
    private String password;
    private String email;

    public User() {

    }

    public User(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }


    public String getObjectId() {
        return "" + objectId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
