package jeroomlamote.be.airsoftarea.Model;

/**
 * Created by jeroom on 26/04/2017.
 */

public class AirsoftLocation {
    private double longitude;
    private double latitude;
    private String creator;

    public AirsoftLocation(double longitude, double latitude, String creator) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.creator = creator;
    }

    public double getLongitude() {
        return longitude;
    }
    public double getLatitude() {
        return latitude;
    }
    public String getCreator() {
        return creator;
    }
}
