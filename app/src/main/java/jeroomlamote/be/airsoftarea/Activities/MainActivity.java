package jeroomlamote.be.airsoftarea.Activities;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import jeroomlamote.be.airsoftarea.Fragments.LoginFragment;
import jeroomlamote.be.airsoftarea.Fragments.RegisterFragment;
import jeroomlamote.be.airsoftarea.R;

public class MainActivity extends AppCompatActivity implements LoginFragment.OnFragmentLoginInteractionListener, RegisterFragment.OnFragmentRegisterInteractionListener {

    //loginfragment laden
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("D", "OnCreateMainActivity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new LoginFragment()).addToBackStack(null)
                    .commit();
        }
    }

    public void navigateFromLoginFragment(){
        Log.d("D", "navigateFromLoginFragmentMainActivity");
        getFragmentManager().beginTransaction().replace(R.id.container, new RegisterFragment()).addToBackStack(null)
                .commit();
    }

    public void navigateFromRegisterFragment(){
        Log.d("D", "navigateFromRegisterFragmentMainActivity");
        getFragmentManager().beginTransaction().replace(R.id.container, new LoginFragment()).addToBackStack(null)
                .commit();
    }

    public void startSecondActivity() {
        Log.d("D", "startSecondActivityMainActivity");
        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);
    }

    public void startSecondActivityWithUsername(String username) {
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("username", username);
        startActivity(intent);
    }
}
