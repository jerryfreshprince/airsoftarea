package jeroomlamote.be.airsoftarea.Activities;

import android.support.annotation.RequiresApi;
import android.annotation.TargetApi;
import android.Manifest;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import com.parse.ParseUser;
import jeroomlamote.be.airsoftarea.Fragments.AddLocationFragment;
import jeroomlamote.be.airsoftarea.Fragments.LocationsListFragment;
import jeroomlamote.be.airsoftarea.Fragments.ProfileFragment;
import jeroomlamote.be.airsoftarea.Helpers.DatabaseHelper;
import jeroomlamote.be.airsoftarea.Model.User;
import jeroomlamote.be.airsoftarea.R;


public class SecondActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ProfileFragment.OnFragmentProfileInteractionListener, AddLocationFragment.OnFragmentAddLocationInteractionListener, LocationsListFragment.OnFragmenLocationsListInteractionListener {

    private TextView tvHeaderName;
    private TextView tvHeaderEmailaddress;
    private FragmentManager fragmentManager;
    private SharedPreferences prefs;
    public User userSqlite;
    private boolean useParse;

    private static final String[] INITIAL_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    private static final int INITIAL_REQUEST = 1337;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        DatabaseHelper myDb = new DatabaseHelper(getApplicationContext());
        userSqlite = new User();
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        fragmentManager = getFragmentManager();
        useParse = prefs.getBoolean("useParse", false);

        //we gebruiken sqlite en vragen de user op aandehand van zijn username
        if (!useParse) {
            String username = getIntent().getStringExtra("username");
            userSqlite = myDb.getUser(username);
        }

        //om later push berichten te displayen
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "A message", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //bij het starten van de activity de drawer al tonen
        drawer.openDrawer(Gravity.LEFT);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);


        tvHeaderName = (TextView) header.findViewById(R.id.nav_header_second_tvHeaderName);
        tvHeaderEmailaddress = (TextView) header.findViewById(R.id.nav_header_second_tvHeaderEmailaddress);

        //header titels veranderen aan de hand van welke opslagmethode we gekozen hebben (Parse of Sqlite)
        if (useParse) {
            ParseUser currentUser = ParseUser.getCurrentUser();
            if (currentUser != null) {
                tvHeaderName.setText(currentUser.getUsername());
                tvHeaderEmailaddress.setText(currentUser.getEmail());
            }
        } else {
            if (userSqlite != null) {
                tvHeaderName.setText(userSqlite.getUsername());
                tvHeaderEmailaddress.setText(userSqlite.getEmail());
            }
        }

        //toegang vragen tot de locatie, de eerste keer je de app start
        if (!canAccessLocation()) {
            requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.second, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //navigeren
        if (id == R.id.profile_settings) {
            startProfileFragment();
        } else if (id == R.id.add_locations) {
            startAddLocationFragment();
        } else if (id == R.id.list_locations) {
            startLocationsListFragment();
        } else if (id == R.id.logout) {
            startMainActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        //navigeren
        if (id == R.id.nav_user) {
            startProfileFragment();
        } else if (id == R.id.nav_location_add) {

            if (canAccessLocation()) {
                startAddLocationFragment();
            } else {
                requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
            }

        } else if (id == R.id.nav_location_list) {
            startLocationsListFragment();
        } else if (id == R.id.nav_logout) {
            startMainActivity();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void startProfileFragment() {
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, new ProfileFragment())
                .commit();
    }

    public void startAddLocationFragment() {
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, new AddLocationFragment())
                .commit();
    }

    public void startLocationsListFragment() {
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, new LocationsListFragment())
                .commit();
    }

    //de eerste keer wanneer men de app instaleert moet de gebruiker de gps locatie toestaan
    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION) && hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case INITIAL_REQUEST:
                if (canAccessLocation()) {
                } else {
                }
                break;
        }
    }

    //als het account aangepast is de titels in de header opnieuw veranderen
    @Override
    public void changeHeaderText() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        tvHeaderName = (TextView) header.findViewById(R.id.nav_header_second_tvHeaderName);
        tvHeaderEmailaddress = (TextView) header.findViewById(R.id.nav_header_second_tvHeaderEmailaddress);
        useParse = prefs.getBoolean("useParse", false);

        if (useParse) {
            ParseUser currentUser = ParseUser.getCurrentUser();
            if (currentUser != null) {
                tvHeaderName.setText(currentUser.getUsername());
                tvHeaderEmailaddress.setText(currentUser.getEmail());
            }
        } else {
            if (userSqlite != null) {
                tvHeaderName.setText(userSqlite.getUsername());
                tvHeaderEmailaddress.setText(userSqlite.getEmail());
            }
        }
    }
}
