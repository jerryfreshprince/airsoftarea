package jeroomlamote.be.airsoftarea.Activities;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.List;
import jeroomlamote.be.airsoftarea.R;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private List<double[]> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Intent i = getIntent();
        list = (List<double[]>) i.getSerializableExtra("location");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //alle geopoints op de kaart zetten
        for(int i = 0 ; i <list.size();i++){
            LatLng temp = new LatLng(list.get(i)[0],list.get(i)[1]);
            mMap.addMarker(new MarkerOptions().position(temp).title("Location from Server"));
            if(i == 0) {
                setCameraToFirstLocation(list.get(i)[0],list.get(i)[1]);
            }
        }
    }

    //inzoomen op de eerste locatie in de list
    private void setCameraToFirstLocation(double v, double v1) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(v, v1), 9.5f));
    }
}
